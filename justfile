set windows-shell := ["pwsh.exe", "-NoLogo", "-Command"]

_default:
    @just --list

install:
    @cargo install --path .